/*
 * Copyright (C) 2013 The DokDo Project - neighbors28
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui;

import android.os.UserHandle;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.provider.Settings;
 
public class HeadsetService extends Service {
 
    private static final String TAG = "AudioService";
    private MusicIntentReceiver myReceiver;
    
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
 
    @Override
    public void onStart(Intent intent, int startId) {
        myReceiver = new MusicIntentReceiver();
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(myReceiver, filter);
        
        super.onStart(intent, startId);
    }
    
    private class MusicIntentReceiver extends BroadcastReceiver {
        @Override public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
        	boolean launchPlayer = Settings.System.getIntForUser(
                    context.getContentResolver(),
                    Settings.System.HEADSET_DETECTOR,
                    0, UserHandle.USER_CURRENT) != 0;
                    DisConnect();
        	if(launchPlayer) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                case 0:
                    DisConnect();
                    break;
                case 1:
                    Connect();
                    break;
                default:
                }
             }
          }
       }
    }
 
    @Override
    public void onDestroy() {
        unregisterReceiver(myReceiver);
        super.onDestroy();
     
    }
    
    private Intent StartApp() {
        NotificationManager notiManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notiManager.cancel(0);

        Intent playerIntent = new Intent(Intent.ACTION_MAIN);
        playerIntent.addCategory(Intent.CATEGORY_APP_MUSIC);
        return playerIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }
    
    private void Connect() {
        PendingIntent pi = PendingIntent.getActivity(this, 0, StartApp(), 0);
        Notification.Builder notiBuilder = new Notification.Builder(getApplicationContext());
        notiBuilder.setSmallIcon(R.drawable.ic_music);
        notiBuilder.setContentTitle((getResources().getString(R.string.plug_on_headset)));
        notiBuilder.setContentText((getResources().getString(R.string.plug_on_headset_summary)));
        notiBuilder.setContentIntent(pi);
        notiBuilder.setAutoCancel(true);

        Notification noti = notiBuilder.build();
       
        NotificationManager notiManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notiManager.notify(0, noti);
    }
    
    private void DisConnect() {
        NotificationManager notiManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notiManager.cancel(0);
    }
}
